<?php

namespace App\Form;

use App\Entity\Course;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du cours',
                'required' => true
            ])
            ->add('smallDescription', TextType::class, [
                'label' => 'Courte description du cours'
            ])
            ->add('fullDescription', CKEditorType::class, [
                'label' => 'Description complète du cours'
            ])
            ->add('duration', NumberType::class, [
                'label' => 'Durée',
                'required' => true
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prix',
                'required' => true
            ])
            ->add('isPublished', ChoiceType::class, [
                'label' => 'Disponibilité',
                'choices' => ['Disponible' => 1, 'Non-disponible' => 0]
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image du cours',
                'required' => false
            ])
//            ->add('schedule', TextType::class, [
//                'label' => 'Horaire'
//            ])
            // Trouver un moyen de faire un multichoice tout en restant en string
            ->add('schedule', ChoiceType::class, [
                'label' => 'schedule',
                'placeholder' => 'Choisissez des valeurs',
                'choices' => ['Lundi' => 'Monday',
                    'Mardi' => 'Tuesday',
                    'Mercredi' => 'Wednesday',
                    'Jeudi' => 'Thursday',
                    'Vendredi' => 'Friday',
                    'Samedi' => 'Saturday',
                    'Dimanche' => 'Sunday'
                ]
            ])
            ->add('programFile', VichFileType::class, [
                'label' => 'Sélectionnez le programme',
                'required' => true
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'placeholder' => 'Sélectionnez une catégorie',
                'class' => 'App:CourseCategory',
//                Trier les catégories
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.name','ASC');
                },
                'choice_label' => 'name'
            ])
            ->add('level', EntityType::class, [
                'label' => 'Niveau',
                'placeholder' => 'Sélectionnez un niveau',
                'class' => 'App:CourseLevel',
                'choice_label' => 'name'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
