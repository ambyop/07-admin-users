<?php

namespace App\Form;

use App\Entity\NewActu;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class NewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'label' => 'Nom de l\'annonce',
                'required' => true
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Contenu de l\'annonce'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image de l\'annonce',
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NewActu::class,
        ]);
    }
}
