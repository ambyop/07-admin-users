<?php

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

//Vich
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

//Validation
use Symfony\Component\Validator\Constraints as Assert;
// Validation uniqueEntity
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 * @Vich\Uploadable()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="Une formation avec le même intitulé a déjà été posté."
 * )
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=CourseCategory::class, inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=CourseLevel::class, inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $level;

    /**
     * @ORM\Column(type="string", length=120)
     * @Assert\Length(
     *     min = 5,
     *     max= 120,
     *     minMessage= "Le nom doit contenir au moins {{ limit }} caractères",
     *     maxMessage= "Le nom ne peut contenir plus de {{ limit }} caractères"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9À-ÿ\-',?!œ ]+$/",
     *     match=true,
     *     message="Uniquement des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     *  @Assert\Length(
     *     min = 10,
     *     max= 300,
     *     minMessage= "La description courte doit contenir au moins {{ limit }} caractères",
     *     maxMessage= "La description courte ne peut contenir plus de {{ limit }} caractères"
     * )
     */
    private $smallDescription;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(
     *     message= "Ce champ ne peut pas être vide"
     * )
     */
    private $fullDescription;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $duration;

    /**
     * @ORM\Column(type="float")
     * @Assert\Range(
     *     min = 50,
     *     max = 500,
     *     notInRangeMessage= "Le prix doit être compris entre {{ min }}€ et {{ max }}€"
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="course_image", fileNameProperty="image")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $schedule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $program;

    /**
     * @Vich\UploadableField(mapping="course_pdf", fileNameProperty="program")
     *
     * @var File|null
     */
    private $programFile;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="course")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=RegistrationCourse::class, mappedBy="course", orphanRemoval=true)
     */
    private $registrationCourses;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->registrationCourses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?CourseCategory
    {
        return $this->category;
    }

    public function setCategory(?CourseCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getLevel(): ?CourseLevel
    {
        return $this->level;
    }

    public function setLevel(?CourseLevel $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSmallDescription(): ?string
    {
        return $this->smallDescription;
    }

    public function setSmallDescription(string $smallDescription): self
    {
        $this->smallDescription = $smallDescription;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(string $fullDescription): self
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    public function setSchedule(string $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getProgram(): ?string
    {
        return $this->program;
    }

    public function setProgram(?string $program): self
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setCourse($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getCourse() === $this) {
                $comment->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return mixed
     */
    public function getProgramFile()
    {
        return $this->programFile;
    }

    /**
     * @param File|null $programFile
     * @throws \Exception
     */
    public function setProgramFile(?File $programFile = null): void
    {
        $this->programFile = $programFile;

        if (null !== $programFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return Collection|RegistrationCourse[]
     */
    public function getRegistrationCourses(): Collection
    {
        return $this->registrationCourses;
    }

    public function addRegistrationCourse(RegistrationCourse $registrationCourse): self
    {
        if (!$this->registrationCourses->contains($registrationCourse)) {
            $this->registrationCourses[] = $registrationCourse;
            $registrationCourse->setCourse($this);
        }

        return $this;
    }

    public function removeRegistrationCourse(RegistrationCourse $registrationCourse): self
    {
        if ($this->registrationCourses->removeElement($registrationCourse)) {
            // set the owning side to null (unless already changed)
            if ($registrationCourse->getCourse() === $this) {
                $registrationCourse->setCourse(null);
            }
        }

        return $this;
    }

}
