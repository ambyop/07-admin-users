<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

//Vich
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

//Validation
use Symfony\Component\Validator\Constraints as Assert;
// Validation uniqueEntity
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

//Captcha
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Vich\Uploadable()
 * @UniqueEntity(
 *     fields={"email"},
 *     message="Un utilisateur avec le même mail existe déjà."
 * )
 */
class User implements UserInterface
{
    /**
     * @Recaptcha\IsTrue
     */
    public $recaptcha;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=120)
     * @Assert\Length(
     *     min = 1,
     *     max= 120,
     *     minMessage= "Le prénom doit contenir au moins {{ limit }} caractères",
     *     maxMessage= "Le prénom ne peut contenir plus de {{ limit }} caractères"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-ZÀ-ÿœ]+$/",
     *     match=true,
     *     message="Uniquement des lettres"
     * )
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=120)
     * * @Assert\Length(
     *     min = 1,
     *     max= 120,
     *     minMessage= "Le nom doit contenir au moins {{ limit }} caractères",
     *     maxMessage= "Le nom ne peut contenir plus de {{ limit }} caractères"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-ZÀ-ÿœ']+$/",
     *     match=true,
     *     message="Uniquement des lettres"
     * )
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="image")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastLogAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDisabled;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="author")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=RegistrationCourse::class, mappedBy="user", orphanRemoval=true)
     */
    private $registrationCourses;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->registrationCourses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
//        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLastLogAt(): ?\DateTimeInterface
    {
        return $this->lastLogAt;
    }

    public function setLastLogAt(\DateTimeInterface $lastLogAt): self
    {
        $this->lastLogAt = $lastLogAt;

        return $this;
    }

    public function getIsDisabled(): ?bool
    {
        return $this->isDisabled;
    }

    public function setIsDisabled(bool $isDisabled): self
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAuthor() === $this) {
                $comment->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function __serialize(): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'roles' => $this->roles,
            'password'=>$this->password,
            'firstname'=>$this->firstname,
            'lastname'=>$this->lastname,
            'image'=>$this->image,
            'createdAt'=>$this->createdAt,
            'updatedAt'=>$this->updatedAt,
            'lastLogAt'=>$this->lastLogAt,
            'isDisabled'=>$this->isDisabled,
            'comments'=>$this->comments
        ];
    }

    public function __unserialize(array $serialized)
    {
        $this->id = $serialized['id'];
        $this->email = $serialized['email'];
        $this->roles = $serialized['roles'];
        $this->password = $serialized['password'];
        $this->firstname = $serialized['firstname'];
        $this->lastname = $serialized['lastname'];
        $this->image = $serialized['image'];
        $this->createdAt = $serialized['createdAt'];
        $this->updatedAt = $serialized['updatedAt'];
        $this->lastLogAt = $serialized['lastLogAt'];
        $this->isDisabled = $serialized['isDisabled'];
        $this->comments = $serialized['comments'];
        return $this;
    }

    /**
     * @return Collection|RegistrationCourse[]
     */
    public function getRegistrationCourses(): Collection
    {
        return $this->registrationCourses;
    }

    public function addRegistrationCourse(RegistrationCourse $registrationCourse): self
    {
        if (!$this->registrationCourses->contains($registrationCourse)) {
            $this->registrationCourses[] = $registrationCourse;
            $registrationCourse->setUser($this);
        }

        return $this;
    }

    public function removeRegistrationCourse(RegistrationCourse $registrationCourse): self
    {
        if ($this->registrationCourses->removeElement($registrationCourse)) {
            // set the owning side to null (unless already changed)
            if ($registrationCourse->getUser() === $this) {
                $registrationCourse->setUser(null);
            }
        }

        return $this;
    }
}
