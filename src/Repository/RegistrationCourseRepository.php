<?php

namespace App\Repository;

use App\Entity\RegistrationCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegistrationCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistrationCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationCourse[]    findAll()
 * @method RegistrationCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationCourse::class);
    }

    // /**
    //  * @return RegistrationCourse[] Returns an array of RegistrationCourse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegistrationCourse
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
