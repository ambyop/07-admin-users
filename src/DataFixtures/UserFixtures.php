<?php

namespace App\DataFixtures;

use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $genders = ['male','female'];
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i=1; $i <= 40; $i++)
        {
            $user = new User();
            $slug = new Slugify();
            $gender = $faker->randomElement($this->genders);
            $user->setFirstname($faker->firstName($gender));
            $user->setLastname($faker->lastName);
            $user->setEmail($slug->slugify($user->getFirstname()).'.'.$slug->slugify($user->getLastname()).'@gmail.com');
            $gender = $gender == 'male' ? 'm' : 'f';
            $user->setImage('0'.($i +10). $gender . '.jpg');
            $user->setPassword($this->encoder->encodePassword($user, 'password'));
            $user->setCreatedAt($faker->dateTimeBetween('-6 months','now'));
            $user->setUpdatedAt($faker->dateTimeBetween('now'));
            $user->setLastLogAt($faker->dateTimeBetween('now'));
            $user->setIsDisabled($faker->boolean(10));
            $user->setRoles(['ROLE_USER']);

            $manager->persist($user);
        }
        /* User role ADMIN */
        $userAdmin = new User();
        $userAdmin->setFirstname('Kovide');
        $userAdmin->setLastname('Dizenoeuf');
        $userAdmin->setEmail('covid19@oms.com');
        $userAdmin->setImage('default.png');
        $userAdmin->setPassword($this->encoder->encodePassword($user, 'password'));
        $userAdmin->setCreatedAt($faker->dateTimeBetween('-6 months','now'));
        $userAdmin->setUpdatedAt($faker->dateTimeBetween('now'));
        $userAdmin->setLastLogAt($faker->dateTimeBetween('now'));
        $userAdmin->setIsDisabled(false);
        $userAdmin->setRoles(['ROLE_ADMIN']);

        $manager->persist($userAdmin);

        /* User role ADMIN */
        $userSuperAdmin = new User();
        $userSuperAdmin->setFirstname('William');
        $userSuperAdmin->setLastname('Wats');
        $userSuperAdmin->setEmail('williamwats@outlook.be');
        $userSuperAdmin->setImage('default.png');
        $userSuperAdmin->setPassword($this->encoder->encodePassword($user, 'password'));
        $userSuperAdmin->setCreatedAt($faker->dateTimeBetween('-9 months','now'));
        $userSuperAdmin->setUpdatedAt($faker->dateTimeBetween('now'));
        $userSuperAdmin->setLastLogAt($faker->dateTimeBetween('now'));
        $userSuperAdmin->setIsDisabled(false);
        $userSuperAdmin->setRoles(['ROLE_SUPER_ADMIN']);

        $manager->persist($userSuperAdmin);


        $manager->flush();
    }
}