<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\RegistrationCourse;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RegistrationCourseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $course = $manager->getRepository(Course::class)->findAll();
        $user = $manager->getRepository(User::class)->findAll();

        for ($i=0; $i<=6; $i++){
            $registration = new RegistrationCourse();
            $registration->setUser($user[$faker->numberBetween(0,count($user)-1)])
                ->setCourse($course[$faker->numberBetween(0,count($course)-1)])
                ->setUpdatedAt($faker->dateTimeBetween('-2 months','now'));

            $manager->persist($registration);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CourseFixtures::class
        ];
    }
}
