<?php

namespace App\Controller\Admin;

use App\Entity\NewActu;
use App\Form\NewEditType;
use App\Form\NewType;
use App\Repository\NewActuRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminNewActuController extends AbstractController
{
    /**
     * @Route("/admin/news", name="admin_news")
     * @param NewActuRepository $repository
     * @return Response
     */

    public function news(NewActuRepository $repository): Response
    {
        return $this->render('admin/admin-news.html.twig',
            [
                'news' => $repository->findAll()
            ]);
    }

    /**
     * @Route("/admin/addnews", name="admin_news_add")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function addNews(EntityManagerInterface $manager, Request $request): Response
    {
        $new = new NewActu();
        $form = $this->createForm(NewType::class, $new);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $new->setCreatedAt(new \DateTime('now', new \DateTimeZone('Europe/Brussels')))
                ->setSlug($slugify->slugify($new->getName()));
            $manager->persist($new);
            $manager->flush();
            $this->addFlash('success', 'L\'actualité ' . $new->getName() . ' a bien été créée.');

            return $this->redirectToRoute('admin_news');
        }

        return $this->render("admin/add-news.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /** @Route("/admin/delnews/{id}", name="admin_news_del")
     * @param NewActu $new
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delCourse(NewActu $new, EntityManagerInterface $manager): Response
    {
        $manager->remove($new);
        $manager->flush();
        $this->addFlash('success', 'L\'annonce ' . $new->getName() . ' a bien été supprimé');
        return $this->redirectToRoute('admin_news');
    }

    /**
     * @Route("/admin/editnews/{id}", name="admin_news_edit")
     * @param NewActu $new
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param NewActuRepository $repository
     */
    public function editCourse(NewActu $new, EntityManagerInterface $manager, Request $request, NewActuRepository $repository)
    {
        $form = $this->createForm(NewEditType::class, $new);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $new->setSlug($slugify->slugify($new->getName()));
            $manager->persist($new);
            $manager->flush();
            $this->addFlash('success', 'Le cours ' . $new->getName() . ' a bien été édité');
            return $this->redirectToRoute('admin_news');
        }
        return $this->render('admin/edit-news.html.twig', [
            'form' => $form->createView(),
            'course' => $new
        ]);
    }
}