<?php

namespace App\Controller\Admin;


use App\Entity\Comment;
use App\Entity\Course;
use App\Entity\User;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommentController extends AbstractController
{
//    Affichage Commentaire

    /**
     * @Route("/admin/comment", name="admin_comment")
     * @param CommentRepository $repoComment
     * @return Response
     */

    public function comments(CommentRepository $repoComment): Response
    {
        return $this->render('admin/admin-comment.html.twig',
            [
                'comments' => $repoComment->findAll()
            ]);
    }
//    /**
//     * @Route("/admin/comment", name="admin_comment")
//     * @param EntityManagerInterface $manager
//     * @param PaginatorInterface $paginator
//     * @param Request $request
//     * @return Response
//     */
//
//    public function comments(EntityManagerInterface $manager, PaginatorInterface $paginator, Request $request): Response
//    {
//        $dqlComment = "SELECT c FROM App:Comment c";
//        $queryComment = $manager->createQuery($dqlComment);
//
//        $comments = $paginator->paginate(
//            $queryComment, /* query NOT result */
//            $request->query->getInt('page', 1), /*page number*/
//            10 /*limit per page*/
//        );
//
//        return $this->render('admin/admin-comment.html.twig',
//            [
//                'comments' => $comments
//            ]);
//    }

    /** @Route("/admin/delcomment/{id}", name="admin_comment_del")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delComment(Comment $comment, EntityManagerInterface $manager): Response
    {
        $manager->remove($comment);
        $manager->flush();
        $this->addFlash('success', 'Le commentaire de ' . $comment->getId() . ' a bien été supprimé');
        return $this->redirectToRoute('admin_comment');
    }

    /**
     * @Route("/admin/warn/{id}", name="admin_comment_warn")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function warn(Comment $comment, EntityManagerInterface $manager): Response
    {
        $comment->setWarning(!$comment->getWarning());
        $manager->flush();
        return $this->redirectToRoute('admin_comment');
    }

    /**
     * @Route("/admin/viewcomment/{id}", name="admin_comment_view")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function view(Comment $comment, EntityManagerInterface $manager): Response
    {
        $comment->setIsDisabled(!$comment->getIsDisabled());
        $manager->flush();
        return $this->redirectToRoute('admin_comment');
    }
}