<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin/user", name="admin_user")
     * @param UserRepository $repository
     * @return Response
     */
    public function users(UserRepository $repository): Response
    {
        return $this->render('admin/admin-user.html.twig',
            [
                'users' => $repository->findAll()
            ]);
    }
//    /**
//     * @Route("/admin/user", name="admin_user")
//     * @param EntityManagerInterface $manager
//     * @param PaginatorInterface $paginator
//     * @param Request $request
//     * @return Response
//     */
//    public function users(EntityManagerInterface $manager, PaginatorInterface $paginator, Request $request): Response
//    {
//        $dql   = "SELECT u FROM App:User u";
//        $query = $manager->createQuery($dql);
//
//        $pagination = $paginator->paginate(
//            $query, /* query NOT result */
//            $request->query->getInt('page', 1), /*page number*/
//            10 /*limit per page*/
//        );
//
//        // parameters to template
//        return $this->render('admin/admin-user.html.twig', ['users' => $pagination]);
//    }


    /**
     * @Route("/admin/viewuser/{id}", name="admin_user_view")
     * @param User $user ;
     * @param EntityManage  rInterface $manager
     * @return Response
     */
    public function viewUser(User $user, EntityManagerInterface $manager): Response
    {
        $user->setIsDisabled(!$user->getIsDisabled());
        $manager->flush();
        return $this->redirectToRoute('admin_user');
    }

    /**
     * @Route("/admin/adduser", name="admin_user_add")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     * @throws \Exception
     */
    public function addUser(EntityManagerInterface $manager, Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setCreatedAt($now)
                ->setLastLogAt($now)
                ->setUpdatedAt($now)
                ->setImage('default.png');
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'L\'utilisateur ' . $user->getFirstname() . ' ' . $user->getLastname() . ' a bien été créé.');

            return $this->redirectToRoute('admin_user');
        }

        return $this->render("admin/add-user.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/promote/{id}/{role}",name="admin_user_promote")
     * @param User $user
     * @param EntityManagerInterface $manager
     * @param $role
     * @return Response
     */
    public function promote(User $user, EntityManagerInterface $manager,$role):Response
    {
        $user->setRoles([$role]);
        $manager->flush();

        $this->addFlash('success', 'L\'utilisateur a bien été promu');
        return $this->redirectToRoute('admin_user');
    }
}