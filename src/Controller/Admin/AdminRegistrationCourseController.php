<?php


namespace App\Controller\Admin;


use App\Entity\RegistrationCourse;
use App\Repository\RegistrationCourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminRegistrationCourseController extends AbstractController
{

    /**
     * @Route("/admin/registrationCourse", name="admin_registration_course")
     *
     * @param RegistrationCourseRepository $repository
     * @return Response
     */
    public function courses(RegistrationCourseRepository $repository): Response
    {
        return $this->render('admin/admin-registrationcourse.html.twig',
            [
                'registrations' => $repository->findAll()
            ]);
    }


    /**
     * @Route("/admin/delregistrationCourse/{id}", name="admin_registration_course_del")
     * @param RegistrationCourse $registrationCourse
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delCourse(RegistrationCourse $registrationCourse, EntityManagerInterface $manager): Response
    {
        $manager->remove($registrationCourse);
        $manager->flush();
        $this->addFlash('success', 'L\'enregistrement ' . $registrationCourse->getId() . ' a bien été supprimé');
        return $this->redirectToRoute('admin_registration_course');
    }
}