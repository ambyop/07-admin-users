<?php

namespace App\Controller;

use App\Entity\RegistrationCourse;
use App\Form\UserEditType;
use App\Form\UserPasswordEditType;
use App\Repository\CommentRepository;
use App\Repository\RegistrationCourseRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     * @IsGranted("ROLE_USER")
     * @param CommentRepository $repoComment
     * @param UserRepository $repoUser
     * @param TokenStorageInterface $tokenStorage
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     * @throws \Exception
     */
    public function profile(CommentRepository $repoComment, UserRepository $repoUser, TokenStorageInterface $tokenStorage, Request $request, EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder): Response
    {

        //Génération Formulaire
        $user = $repoUser->find($tokenStorage->getToken()->getUser());
        $formProfile = $this->createForm(UserEditType::class,$user);
        $formProfile->handleRequest($request);
        if ($formProfile->isSubmitted() && $formProfile->isValid()) {
            $user->setUpdatedAt(new \DateTime('now', new \DateTimeZone('Europe/Brussels')));
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'Votre profil a bien été modifié');
            return $this->redirectToRoute('user');
        }
        $formPassword = $this->createForm(UserPasswordEditType::class,$user);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'Votre mot de passe a bien été modifié.');
            return $this->redirectToRoute('user');
        }
        // Récupération Commentaire
        $comments =  $repoComment->findBy(
            ['author' => $tokenStorage->getToken()->getUser()],
            ['createdAt' => 'DESC']
        );
        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'comments' => $comments,
            'formProfile' => $formProfile->createView(),
            'formPassword' => $formPassword->createView()
        ]);
    }


    /**
     * @Route("/user/cart", name="cart")
     * @IsGranted("ROLE_USER")
     * @param EntityManagerInterface $manager
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param UserRepository $repoUser
     * @param TokenStorageInterface $tokenStorage
     * @return Response
     */
    public function cart(RegistrationCourseRepository $repoRegistration,UserRepository $repoUser,TokenStorageInterface $tokenStorage):Response
    {
        $user = $repoUser->find($tokenStorage->getToken()->getUser());
        // trouver le panier
        $registration =  $repoRegistration->findBy(
            ['user' => $user],
            ['updatedAt' => 'DESC']
        );


        return $this->render('user/cart.html.twig', [
            'registrations' => $registration
        ]);
    }

    /**
     * @Route("/user/cart/delregistrationCourse/{id}", name="cart_del")
     * @param RegistrationCourse $registrationCourse
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delCourse(RegistrationCourse $registrationCourse, EntityManagerInterface $manager): Response
    {
        $manager->remove($registrationCourse);
        $manager->flush();
        return $this->redirectToRoute('cart');
    }
}
