<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Entity\RegistrationCourse;
use App\Form\CommentEditType;
use App\Form\CommentType;
use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use App\Repository\RegistrationCourseRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CourseController
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/courses", name="courses")
     * @param CourseCategoryRepository $repoCategory
     * @param CourseRepository $repoCourse
     * @return Response
     */
    public function courses(CourseCategoryRepository $repoCategory, CourseRepository $repoCourse): Response
    {
        $courses = $repoCourse->findBy(
            ['isPublished' => true],
            ['name' => 'ASC']
        );
        $categories = $repoCategory->findBy(
            [],
            ['name' => 'ASC']
        );
        return $this->render('course/courses.html.twig', [
            'courses' => $courses,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/course/{slug}", name="course")
     * @param Course $course
     * @param RegistrationCourseRepository $repoRegister
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws \Exception
     */
    // Uitilisation du composant ParamConverter
    public function course(Course $course,RegistrationCourseRepository $repoRegister,Request $request, EntityManagerInterface $manager): Response
    {
        // Formulaire de postComment
        $register = $repoRegister->findBy([
            'course'=> $course->getId()
        ]);
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setCourse($course);
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $comment->setCreatedAt($now);
            $comment->setIsDisabled(0);
            $comment->setWarning(0);
            $manager->persist($comment);
            $manager->flush();
        }

        return $this->render('course/detail.html.twig', [
            'registers' => $register,
            'course' => $course,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/course/editcomment/{id}",name="comment_edit_course")
     * @param Comment $comment
     * @param CourseRepository $repoCourse
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function editCommentCourse(Comment $comment,CourseRepository $repoCourse, Request $request,EntityManagerInterface $manager):Response
    {
        $course = $repoCourse->find($comment->getCourse());
        $slug = $course->getSlug();
        $form = $this->createForm(CommentEditType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('course', array('slug' => $slug));
        }

        return $this->render('course/edit-comment.html.twig', [
            'comment' => $comment,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/profile/editcomment/{id}",name="comment_edit_profile")
     * @param Comment $comment
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function editCommentProfile(Comment $comment, Request $request,EntityManagerInterface $manager):Response
    {
        $form = $this->createForm(CommentEditType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('user');
        }

        return $this->render('course/edit-comment.html.twig', [
            'comment' => $comment,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/course/register/{id}",name="register_course")
     * @param UserRepository $repoUser
     * @param Course $course
     * @param TokenStorageInterface $tokenStorage
     * @return Response
     * @throws \Exception
     */
    public function registerCoure(UserRepository $repoUser,Course $course, TokenStorageInterface $tokenStorage,EntityManagerInterface $manager): Response
    {
        $user = $repoUser->find($tokenStorage->getToken()->getUser());
        $register = new RegistrationCourse();
        $register->setUpdatedAt(new \DateTime('now', new \DateTimeZone('Europe/Brussels')))
            ->setUser($user)
            ->setCourse($course);
        $manager->persist($register);
        $manager->flush();
        $this->addFlash('success', 'Vous avez été inscrit avec succès');

        return $this->redirectToRoute('course', array('slug' => $course->getSlug()));
    }

}
